# https://pyneng.readthedocs.io/ru/latest/exercises/07_exercises.html
# Задание 7.1
# Обработать строки из файла ospf.txt и вывести информацию
# по каждой строке в таком виде на стандартный поток вывода:
#
# Prefix                10.0.24.0/24
# AD/Metric             110/41
# Next-Hop              10.0.13.3
# Last update           3d18h
# Outbound Interface    FastEthernet0/0

list_name = ['Prefix', 'AD/Metric', 'Next-Hop', 'Last update', 'Outbound Interface']

with open('ospf.txt', 'r') as f:
    for line in f:

        line = line.split()
        line[2] = line[2].strip('[]')
        line[4] = line[4].strip(',')
        line[5] = line[5].strip(',')
        line.pop(0)
        line.pop(2)

        for i in range(line.__len__()):
            print('{:25}{}'.format(list_name[i], line[i]))

